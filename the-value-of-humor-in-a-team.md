title: The Value of humor in a Team
summary: How a good sense of humor can improve a mood in an office environment.

Disclaimer: This is just some thoughts that came to my mind when I think about how humor makes us better on coping with real, serious problems or when we need to break the ice between two co-workers.

"Humour is not just frivolous entertainment - it can help us cope with situations that are overwise impossible to understand" I got this sentence from a [BBC article](http://www.bbc.com/future/story/20160829-how-laughter-makes-us-better-people). I realized that humor isn't just a silly thing when I saw a how my co-worker, who is really funny, played jokes on everyone even though he was a boss, because as a leader he knew that people work better when they free their minds from worries and focus on the problem.

Since then I have begun to use this to help me to create a really good mood in the office, so we can face the problems without any other worries than those that came with the problem.

There are some issues that could happen to you when you are trying this technique.
Your colleagues could think that you don't take your job seriously and, to show them the opposite, you need to balance the frequency that you make those jokes. In the beginning leave the jokes for moments when you see that people are also taking their time to relax. Over time, you are going to be more sensitive to the right moment to do it, and when you are facing a problem, act seriously, because sometimes there is no place for jokes.

Here is another tip: don't laugh at me, laugh with me. Of all the advice, this the most important rule. It's tempting to make jokes about the differences between us that is because it's too easy to do it, but please avoid those kinds of jokes.

I'm not trying to say that you need to memorize every joke that you liked or you've seen on TV, because over the time you are going to lost your funny. Try to get some funny situations that you've been through or try to tell a joke as it happens with you, jokes don't need to be true.

I believe this tips could improve the mood of your office and your home, so let the humor make things better.
