title: Ansible for the win
summary: How to use ansible to avoid repetable instalation and configuration process.


# Ansible for the win

I've been a happy Puppet user until I came across Ansible, once I manage to learn it, I move everything to Ansible. The reason why I moved is so simple, I can run any playbooks remotely, through a ssl connection (of course).

Even without mention the idempotence ideology, which also rule how you change the target state of each machine you manage through a software that allow you to write infrastructure as code.

I don't know if you, had the experience to install the all the puppet components on each machine you manage, that simple task waste a long time and end up dirting my machine with software that doesn't need to be installed there.

Ansible script is called *playbook*, it is a so simple yaml file, that I'm sure you won't miss any old style playbook or recipe. Anyway, you will still have the possibility to configure modules to create users, copy files that can be modified using a template engine like [jinja](http://jinja.pocoo.org/), install packages, ensure services are running and, so on. You'll need a hosts file to tell which machine ansible will to connect, after that, you'll only need to run `ansible-playbook -i hosts my-yaml-playbook-file.yaml` at your terminal.

As you improve the complexity of your playbooks you'll feel the need to store your passwords using a more secure solution, anyhow, ansible vault will help you. Again it's simple, you can reference environment variables from text files and, the recommended approach to use ansible-vault is:

Create a text file:

_/var.yml_


	app_user: 'csantos'
	app_pass: '{{ vault_app_password }}'


then, at your terminal you will type `ansible-vault create vault.yml`, a editor like vim will be opened, and you'll type `vault_app_password: 'my_secret_password'`, then save, close and that's it. Remember to reference those two var files, var.yml and vault.yml on your playbook.

Take a quickly look on [this repo](https://gitlab.com/simplologia/website), at the `env/` folder, to see how things can be done.


I hope you enjoyed reading and, any questions please send me an [e-mail](mailto:contato@simplologia.com).