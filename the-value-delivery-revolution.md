title: The value delivery revolution
summary: How delivering value resahped the software development market


# The value delivery revolution

Disclaimer: this is a Brazilian software developer point of view.

I lived the process of changing from formal process to agile. Before that time we was wondered about the "precision" that those practices gave to us, we plan ahead every detail as we were build a house, afterwards we discover that those kinds of plans was the mistake itself in software development area.
At that time, it was usual to see dozens of UML Diagrams, MS Project's plans and Specs that shows in details how our system will behave, but in practice we don't have any usable system, just papers.
Around the same time a group of skilled software developers get together and wrote the Agile Manifesto.
Then we start to see a little change, managers was struggling to follow the movement and the developers got their voice heard and they started to claim for better work conditions and a honest software development process. but we were still focus on how to build things instead of how we can deliver products. Unfortunately this took a bunch of time to be comprehended that we needed to focus on delivering a minimal viable product.

Once we finally had a glance about the power of value delivery, I started to see a real changing in how traditional companies operates, those new ones was born with this concept and unfortunately we have not saw the same at the Government side.

Even if this statement "Our highest priority is to satisfy the customer through early and continuous delivery of valuable software." is the first one of twelve agile principles, we took a long time to change our ecosystem and embrace this concept.

I took a very long path to learn TDD, CI and CD. All of this concepts guide us to improve our delivery's pace, delivering valuable software rapidly and with good quality, although with a reasonable maintenance cost.

## The very good second wave of agility

All of this improvements are useless when you need to come back to a traditional process when you need deploy a software in production. Then we began to see another changing while trying to reach the valuable software delivery path. It's usual to see a formal process, full of authorization steps to finally deploy a software in production and we are seeing a changing again, many companies broke its organization to put together SW DEV team with DBAS and System Admins. The DEVOPS process is above this article's objective, but it's important to illustrate another change guided by the need to improve the pace torwards to a valuable software delivery.

Nowadays we are seeing again another change, because its not enough to deliver a huge monolith in production, actually we need to break it apart to increase the delivery pace and also to facilitate the integration between diferent departments and business. The Air line company need to notify your Google calendar, a Chatbot need to book a flight or a hotel, a retail company need to order products to its partners, and so on.

Again we have seing some new buzz words like API Gateway, Service Discovery, Service Mesh and Side Car. That guarantee security and less bureaucracy, to facilitate the integration process and again the delivery of a valuable software.

This is just a sort of changes that have happened during the last decades in a field which the result is a product that tried to improve the peoples life or business process.

This is indeed the Goal to follow and every time we face the question on which steps to take, you will be sure and confident about your choice if its based on value delivery and keep in mind, Software Development market changed completely because of that reason.








